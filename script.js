// Setup angular
var app = angular.module( "app", [] );

app.controller(
	"paintingCtrl",
	function( $scope )
	{
		$scope.client = new Painting_Client();
	}
);

app.directive( 
	"painting",
	function()
	{
		var directive_object = 
		{
			restrict: "A", // Attribute only
			link: function postLink( scope, element, attributes )
			{
				// Hand the canvas over to our client
				scope.client.set_canvas( element[0].getContext('2d') );
				
				// Setup event handlers for the canvas here
				element.bind(
					'mousedown', 
					function( event )
					{
						if( scope.client.is_drawing() )
							return;
							
						scope.client.start_drawing( event.offsetX, event.offsetY );
					}
				);
				
				element.bind(
					'mouseup',
					function( event )
					{
						// Don't even check, just end it
						scope.client.stop_drawing();
					}
				);
				
				element.bind(
					'mousemove',
					function( event )
					{
						if( scope.client.is_drawing() != true )
						{
							console.log( 'Could not add a stroke, client was not ready to draw' );
							return;
						}
							
						scope.client.add_stroke( event.offsetX, event.offsetY );
					}
				);
			}
		};
	
		return directive_object;
	
	} // End of directive function

); // End of directive definition
