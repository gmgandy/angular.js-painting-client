test( 
	"Painting_Client tests", 
	function() 
	{
		// Init the client
		var client = new Painting_Client();
	
		// Is the drawing state initially set to null?
		ok( client.is_drawing() == false, "Drawing state is initially false : pass" );
		
		// Can we change the drawing state
		client.set_drawing( true );
		ok( client.is_drawing() == true, "Drawing state can be changed : pass" );
		
		// Make sure start_drawing() tests for a valid canvas
		client.set_drawing( false );
		ok( client.start_drawing( 5, 5 ) == false, "start_drawing() checks for a valid canvas : pass" );
		
		// Make sure start_drawing() throws an exception of is_drawing is true
		client.set_drawing( true );
		ok( client.start_drawing( 5, 5 ) == false, "start_drawing() checks is_drawing() : pass" );
	}
);