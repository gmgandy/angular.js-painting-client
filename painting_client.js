var Painting_Client = function()
{
	this.drawing = false;  // True when the client is changing pixels on the canvas
	this.canvas = null;
	this.previous_x = 0;
	this.previous_y = 0;
}

Painting_Client.prototype.is_drawing = function()
{
	return this.drawing;
}

Painting_Client.prototype.set_drawing = function( value )
{
	this.drawing = value;
}

Painting_Client.prototype.set_canvas = function( canvas_2D_context )
{
	this.canvas = canvas_2D_context;
}

Painting_Client.prototype.start_drawing = function( start_x, start_y )
{
	if( this.canvas == null )
	{
		return false;
	}

	if( this.is_drawing() == true )
	{
		return false;
	}

console.log( 'Start stroke at x:' + start_x + ', y:' + start_y );
	
	this.previous_x = start_x;
	this.previous_y = start_y;
	this.set_drawing( true );
	this.canvas.beginPath();
	
	return true;
}

Painting_Client.prototype.stop_drawing = function()
{
	this.set_drawing( false );
}

Painting_Client.prototype.add_stroke = function( next_x, next_y )
{
	if( this.is_drawing() == false )
		return false;

	if( this.canvas == null )
		return false;

	this.canvas.moveTo( this.previous_x, this.previous_y );
	this.canvas.lineTo( next_x, next_y );
	this.canvas.strokeStyle = "#4bf";
	this.canvas.stroke();
	
	console.log( "Adding stroke towards next_x: " + next_x + ", next_y:" + next_y );
	
	this.previous_x = next_x;
	this.previous_y = next_y;
}